#include <ESP8266WiFi.h>
#include <Adafruit_MMA8451.h>
#include <Adafruit_Sensor.h>

Adafruit_MMA8451 mma;

#define SSID "FABLABKAMAKURA2"
#define PASS "kamakura_1192"
#define HOST "api.thingspeak.com"
#define THINGSPEAK_KEY "xxxxxxxx"
#define CHANNEL "xxxxxxxx"

void connectWiFi(){
  WiFi.begin(SSID, PASS);
  while(WiFi.status() != WL_CONNECTED){
    delay(500); Serial.print(".");
  }
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void setup(){
  Serial.begin(9600);
  connectWiFi();
  mma.setRange(MMA8451_RANGE_2_G);
}

void loop() {
  WiFiClient client;     
  const int httpPort = 80;
    if (!client.connect(HOST,httpPort)){
    Serial.println("connection failed");
    return;
  }
  delay(10);

  mma.read();
  int x = mma.x;
  int y = mma.y;
  int z = mma.z;
  String str_x = String(x);
  String str_y = String(y);
  String str_z = String(z);

  String url = "/update?key=";
  url += THINGSPEAK_KEY;
  url += "&field1=";
  url += str_x;
  url += "&field2=";
  url += str_y;
  url += "&field3=";
  url += str_z;


  client.print(String("GET ")
    + url
    + " HTTP/1.1\r\n"
    + "Host: "
    + HOST
    + "\r\n"
    + "Connection: close\r\n\r\n");
  delay(10);

  while(client.available()){
    String line = 
client.readStringUntil('\r');
    Serial.print(line);
  }
  delay(3000);
}
