#include <ESP8266WebServer.h>

#define LED_A 12
#define LED_B 13

#define SSID "FABLABKAMAKURA2"
#define PASS "kamakura_1192"

ESP8266WebServer server(80);

String html = ""
"<!DOCTYPE html>"
"<html>"
"<head>"
"<meta charset='utf-8'>"
"<meta name='viewport' content='width=device-width'>"
"</head>"
"<body>"
"<h1><a href='/both'>Both</a></h1>"
"<h1><a href='/leda'>LEDA</a></h1>"
"<h1><a href='/ledb'>LEDB</a></h1>"
"<h1><a href='/off'>OFF</a></h1>"
"</body>"
"</html>";

void setup(){
  pinMode(LED_A, OUTPUT);
  pinMode(LED_B, OUTPUT);
  digitalWrite(LED_A, LOW);
  digitalWrite(LED_B, LOW);

  Serial.begin(9600);

  initWiFi();
  server.begin();
  Serial.println("HTTP Server started");
  
  server.on("/",     handleRoot);
  server.on("/both", switchOnBoth);
  server.on("/leda", switchOnLedA);
  server.on("/ledb", switchOnLedB);
  server.on("/off",  switchOffLed);
}

void loop(){
  server.handleClient();
}

void initWiFi(){
  WiFi.begin(SSID, PASS);
  Serial.println();
  while(WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.print("WiFi connected");
  Serial.println(WiFi.localIP());
}

void handleRoot(){
  server.send(200, "text/html", html);
  Serial.println("root directory");
}

void switchOnBoth(){
  server.send(200, "text/html", html);
  digitalWrite(LED_A, HIGH);
  digitalWrite(LED_B, HIGH);
  Serial.println("switch on both");
}

void switchOnLedA(){
  server.send(200, "text/html", html);
  digitalWrite(LED_A, HIGH);
  digitalWrite(LED_B, LOW);
  Serial.println("switch on led A");
}

void switchOnLedB(){
  server.send(200, "text/html", html);
  digitalWrite(LED_A, LOW);
  digitalWrite(LED_B, HIGH);
  Serial.println("switch on led B");
}

void switchOffLed(){
  server.send(200, "text/html", html);
  digitalWrite(LED_A, LOW);
  digitalWrite(LED_B, LOW);
  Serial.println("switch off");
}

